# ReVISION docs

This documents what is currently running on UiB infrastructure and on Vercel. 

## Datamodel

Omeka S is an application that can take an [RDF datamodel](https://en.wikipedia.org/wiki/Resource_Description_Framework) and create a editor interface. The datamodel is based on the `bdm.ttl` file in this repository. 

That RDF datamodel is based on the ER diagram `ERD_Draft_1.3.pdf` created by the project.

The datamodell is heavily inspired by [CIDOC-CRM](https://en.wikipedia.org/wiki/CIDOC_Conceptual_Reference_Model). CIDOC-CRM models cultural artifacts and with extensions also texts, its variations and objects carrying these texts. See the resources below for more information about the inspiration behind the work, text, variation model:

*  https://www.cidoc-crm.org/crmtex/home-8
*  https://epigraphy.info/ontologies_wg/

## ReVISION Apps 

The main application used in this project is Omeka S. Two support application for visualization have been added, along with a endpoint for querying the data. 

### ReVISION Omeka

Omeka was chosen after some test using tools the Special collections at the University of Bergen Library (UBB) failed. The project members had some previous experience with Omeka. 

It is a PHP application. UBB have customized a theme for ReVISION: https://git.app.uib.no/revision/theme

* Production
  * https://birgitta.uib.no/s/birgitta/page/home
* Test
  * https://birgitta.test.uib.no/s/birgitta/page/home
* API (see "Omeka S API" below for more information)
  * Documentation: https://omeka.org/s/docs/developer/api/
  * Endpoint: https://birgitta.uib.no/api/ and https://birgitta.test.uib.no/api/

Omeka was originally a simple web exhibition webapp, but the "S" version have support for custom datamodels. It is worth noting that is weak at data validation, uses can easily create data that is inconsistent with the datamodel. Meaning that a field that is supposed to be a reference, can contain a string. This can make it hard to use the data in other applications. 

### ReVISISON Sparql endpoint

Omeka serves its data as RDF, but does not offer its own [SPARQL](https://www.w3.org/TR/sparql11-overview/) endpoint for querying the data. UBB have set up a [Fuseki](https://jena.apache.org/documentation/fuseki2/) endpoint and transfers the data from Omeka here. (Unsure how often the data is synced.)

The SPARQL endpoint is used for the visualizations because of the possibility to find relations and manipulate the data before sending it to a frontend app. 

* Fuseki GUI
  * https://sparql.birgitta.uib.no/#/
* Yasgui (alternative query GUI, with "starter" query)
  * https://api.triplydb.com/s/ghWO_swQdB
* Query Endpoints
  * https://sparql.birgitta.uib.no/birgitta-revision/query
  * https://sparql.birgitta.uib.no/birgitta-revision-test/query

### ReVISION Visualization

This app is an attempt to create a network visualization on the data created in Omeka. The `main` branch was an early React app, but the `next` branch is the most up to date one. 

* https://git.app.uib.no/revision/revision-visualization/-/tree/next/
  * Active branch: `next`
* Deployed to Vercel
  * https://revision-visualization.vercel.app/networks
  * Working visualization: https://revision-visualization.vercel.app/networks/works-books
  * Not working: https://revision-visualization.vercel.app/networks/gifts-donations


### ReVISION Ontodia data visualization

The datamodel and data can be hard to reason about and UBB added another tool to explore the relations in the dataset called [Ontodia](https://github.com/metaphacts/ontodia). The diagram created by this tool can be "stored" via a short url like so: [https://s.zazuko.com/2bBeM9t](https://s.zazuko.com/2bBeM9t). Changing configuration it is also capable of creating SVG files for download.

* https://git.app.uib.no/revision/revision-ontodia
    * Active branch: `main`


## Omeka S API

The Omeka API have several endpoint. One thing to note is that is seems to combine data used for the frontend, with the actual data as well.

| Resource                                                             | Description                                                           |
|----------------------------------------------------------------------|-----------------------------------------------------------------------|
| [users](https://birgitta.uib.no/api/users                            | Users who have login credentials                                      |
| [vocabularies](https://birgitta.uib.no/api/vocabularies              | RDF vocabularies imported into Omeka                                  |
| [resource_classes](https://birgitta.uib.no/api/resource_classes      | RDF classes that belong to vocabularies                               |
| [properties](https://birgitta.uib.no/api/properties                  | RDF properties that belong to vocabularies                            |
| [items](https://birgitta.uib.no/api/items                            | Item RDF resources, the building blocks of Omeka                      |
| [media](https://birgitta.uib.no/api/media                            | Media RDF resources that belong to items                              |
| [item_sets](https://birgitta.uib.no/api/item_sets                    | Item set RDF resources, inclusive set of items                        |
| [resource_templates](https://birgitta.uib.no/api/resource_templates  | Templates that define how to describe RDF resources                   |
| [sites](https://birgitta.uib.no/api/sites                            | Omeka sites, the public components of Omeka                           |
| [site_pages](https://birgitta.uib.no/api/site_pages                  | Pages within sites                                                    |
| [modules](https://birgitta.uib.no/api/modules                        | Modules that extend Omeka fuctionality (search and read access only)  |
| [api_resources](https://birgitta.uib.no/api/api_resources            | API resources available on this install (search and read access only) |

Example respons about the "Sheen Charterhouse", an Institution. 

```json
{
    "@context": "https://birgitta.uib.no/api-context",
    "@id": "https://birgitta.uib.no/api/items/5",
    "@type": [
        "o:Item",
        "bdm2:Institution"
    ],
    "o:id": 5,
    "o:is_public": true,
    "o:owner": {
        "@id": "https://birgitta.uib.no/api/users/13",
        "o:id": 13
    },
    "o:resource_class": {
        "@id": "https://birgitta.uib.no/api/resource_classes/974",
        "o:id": 974
    },
    "o:resource_template": {
        "@id": "https://birgitta.uib.no/api/resource_templates/17",
        "o:id": 17
    },
    "o:thumbnail": null,
    "o:title": "Sheen Charterhouse",
    "thumbnail_display_urls": {
    "large": null,
    "medium": null,
    "square": null
    },
    "o:created": {
        "@value": "2020-09-02T08:20:51+00:00",
        "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
    },
    "o:modified": {
        "@value": "2021-11-26T10:15:22+00:00",
        "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
    },
    "o:media": [],
    "o:item_set": [],
    "o:site": [
        {
        "@id": "https://birgitta.uib.no/api/sites/1",
        "o:id": 1
        }
    ],
    "bdm2:name": [
        {
        "type": "literal",
        "property_id": 1873,
        "property_label": "Name",
        "is_public": true,
        "@value": "Sheen Charterhouse"
        }
    ],
    "bdm2:hasType": [
        {
        "type": "resource",
        "property_id": 1709,
        "property_label": "Has type",
        "is_public": true,
        "@id": "https://birgitta.uib.no/api/items/1",
        "value_resource_id": 1,
        "value_resource_name": "items",
        "url": null,
        "display_title": "[Monastery]"
        }
    ],
    "bdm2:location": [
        {
        "type": "resource:item",
        "property_id": 1711,
        "property_label": "Location",
        "is_public": true,
        "@id": "https://birgitta.uib.no/api/items/1375",
        "value_resource_id": 1375,
        "value_resource_name": "items",
        "url": null,
        "display_title": "Isleworth, UK"
        }
    ],
    "geo:lat": [
        {
        "type": "literal",
        "property_id": 1868,
        "property_label": "latitude",
        "is_public": true,
        "@value": "51,44"
        }
    ],
    "geo:long": [
        {
        "type": "literal",
        "property_id": 1870,
        "property_label": "longitude",
        "is_public": true,
        "@value": "-0,27"
        }
    ]
}

```

## ReVISION and Vercel

`revision-visualization` and `revision-ontodia` is deployed to Vercel.com. This was done to speed up the develoment cycle. Both apps are deployed on the `uib-ub` team: https://vercel.com/uib-ub. Since UBB is not an enterprise customer at Vercel and is ITA as UiB is using the community Gitlab application, we cannot deploy from https://git.app.uib.no, but must use Github.com or Gitlab.com to be able to connect the git repositories to Vercel.com. The deployment-repositories are found on Tarje Laviks personal account, but one suggestion would be to create a `revision` organisation on Github.com and move the repositories there.

* https://github.com/tarjelavik/revision-visualization
* https://github.com/tarjelavik/revision-ontodia
 
## Gephi SPARQL queries

[Gephi](https://gephi.org/) is an open-source network analysis and visualization software package written in Java on the NetBeans platform. Use of Gephi was explored, but was not used. It might be an alternative yet.
 
With the extension [Semantic Web Import](https://github.com/gephi/gephi/wiki/SemanticWebImport), Gephi can connect to the ReVISION SPARQL endpoint and gather nodes and edges.

Introduction on how to use the extension: [Lets play gephi...](http://matthieu-totet.fr/Koumin/2015/09/06/lets-play-gephi-dbpedia-rdf-sparql-and-your-favorite-actors/).

```sparql
CONSTRUCT{
  ?person <http://gephi.org/type> "person".
  ?movie <http://gephi.org/type> "movie".
  ?movie <http://toto/link> ?person .
}
WHERE
{
 ?movie a <http://dbpedia.org/ontology/Film>.
 ?movie ?rel1 <http://dbpedia.org/resource/Leonardo_DiCaprio>.
 ?movie ?rel2 ?person.
 ?person a <http://dbpedia.org/ontology/Person>.
}
```
